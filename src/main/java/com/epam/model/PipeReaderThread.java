package com.epam.model;

import java.io.PipedReader;

public class PipeReaderThread implements Runnable {
    PipedReader pr;
    String name = null;

    public PipeReaderThread(String name, PipedReader pr) {
        this.name = name;
        this.pr = pr;
    }

    public void run() {
        try {
            while (true) {
                char c = (char) pr.read();
                if (c != -1) {
                    System.out.print(c);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
