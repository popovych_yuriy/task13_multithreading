package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizeDifferentMod {
    private static Logger logger = LogManager.getLogger(SynchronizeOne.class);
    private Lock one = new ReentrantLock();
    private Lock two = new ReentrantLock();
    private Lock three = new ReentrantLock();

    public void first() {
        one.lock();
        logger.info("First method start " + Thread.currentThread().getName());
        logger.info("First method finish " + Thread.currentThread().getName());
        one.unlock();
    }

    public void second() {
        two.lock();
        logger.info("Second method start " + Thread.currentThread().getName());
        logger.info("Second method finish " + Thread.currentThread().getName());
        two.unlock();
    }

    public void third() {
        three.lock();
        logger.info("Third method start " + Thread.currentThread().getName());
        logger.info("Third method finish " + Thread.currentThread().getName());
        three.unlock();
    }
}
