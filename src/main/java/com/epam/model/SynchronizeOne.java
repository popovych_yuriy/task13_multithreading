package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOne {
    private static Logger logger = LogManager.getLogger(SynchronizeOne.class);
    private Object object = new Object();

    public void first(){
        synchronized (object){
            logger.info("First method start " + Thread.currentThread().getName());
            logger.info("First method finish " + Thread.currentThread().getName());
        }
    }
    public void second(){
        synchronized (object){
            logger.info("Second method start " + Thread.currentThread().getName());
            logger.info("Second method finish " + Thread.currentThread().getName());
        }
    }
    public void third(){
        synchronized (object){
            logger.info("Third method start " + Thread.currentThread().getName());
            logger.info("Third method finish " + Thread.currentThread().getName());
        }
    }
}
