package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static Logger logger = LogManager.getLogger(PingPong.class);
    private volatile Integer counter;

    void run(int n) {
        counter = n;
        Thread first = new Thread(() -> {
            synchronized (counter) {
                for (int i = 0; i < counter; i++) {
                    try {
                        Thread.sleep(500);
                        counter.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info(Thread.currentThread().getName());
                    counter.notify();
                }
            }
        });
        Thread second = new Thread(() -> {
            synchronized (counter) {
                for (int i = 0; i < counter; i++) {
                    counter.notify();
                    try {
                        Thread.sleep(500);
                        counter.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info(Thread.currentThread().getName());
                }
            }
        });
        first.start();
        second.start();
    }
}
