package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizeOneMod {
    private static Logger logger = LogManager.getLogger(SynchronizeOneMod.class);
    private Lock lock = new ReentrantLock();

    public void first(){
        lock.lock();
        logger.info("First method start " + Thread.currentThread().getName());
        logger.info("First method finish " + Thread.currentThread().getName());
        lock.unlock();
    }
    public void second(){
        lock.lock();
        logger.info("Second method start " + Thread.currentThread().getName());
        logger.info("Second method finish " + Thread.currentThread().getName());
        lock.unlock();
    }
    public void third(){
        lock.lock();
        logger.info("Third method start " + Thread.currentThread().getName());
        logger.info("Third method finish " + Thread.currentThread().getName());
        lock.unlock();
    }
}
