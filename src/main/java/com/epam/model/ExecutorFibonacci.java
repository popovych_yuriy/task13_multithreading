package com.epam.model;

import java.util.concurrent.ExecutorService;

public class ExecutorFibonacci {
    void run(ExecutorService e, int number) {
        for (int i = 1; i <= number; i++) {
            e.execute(new Fibonacci(i));
        }
        e.shutdown();
    }
}
