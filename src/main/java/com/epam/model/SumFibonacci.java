package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class SumFibonacci implements Callable<Integer> {
    private static Logger logger = LogManager.getLogger(com.epam.model.Fibonacci.class);
    List<Integer> numbers = new ArrayList<>();
    private int size;

    public SumFibonacci(int size) {
        this.size = size;
    }

    private int calculate(int n) {
        if (n < 2) {
            numbers.add(n);
            return n;
        } else {
            numbers.add(numbers.get(n - 2) + numbers.get(n - 1));
            return n;
        }
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += calculate(i);
        }
        logger.info("Fibonacci numbers for " + Thread.currentThread().getName() + numbers);
        return sum;
    }
}
