package com.epam.model;

import java.io.PipedWriter;

public class PipeWriterThread implements Runnable {
    PipedWriter pw;
    String name = null;

    public PipeWriterThread(String name, PipedWriter pw) {
        this.name = name;
        this.pw = pw;
    }

    public void run() {
        try {
            while (true) {
                pw.write("Testing data written...");
                pw.flush();
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
