package com.epam.model;

import com.epam.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepRandom extends Thread{
    private static Logger logger = LogManager.getLogger(SleepRandom.class);
    private int quantity;
    private Random random;
    ScheduledExecutorService service;

    public SleepRandom(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public void run(){
        service = Executors.newScheduledThreadPool(quantity);
        for (int i = 0; i < quantity; i++) {
            int sleep = new Random().nextInt((10)+1);
            service.schedule(() -> logger.info(Thread.currentThread().getName()+" Sleep: "+ sleep), sleep, TimeUnit.SECONDS);
        }
    }
}
