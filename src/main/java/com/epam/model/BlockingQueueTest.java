package com.epam.model;

import com.epam.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueTest {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private BlockingQueue<Character> blockingQueue;
    private Thread send;
    private Thread receive;

    public BlockingQueueTest() {
        blockingQueue = new PriorityBlockingQueue<>();
        send = new Thread(this::send);
        receive = new Thread(this::receive);
    }

    public void startCommunication() {
        send.start();
        receive.start();

        try {
            send.join();
            receive.interrupt();
            receive.join();
        } catch (InterruptedException ignored) {}
    }

    private void send() {
        try {
            for (char i = 'A'; i <= 'Z'; i++) {
                Thread.sleep(300);
                blockingQueue.put(i);
                logger.info("Sent: " + i);
            }
        } catch (InterruptedException ignored) { }
        logger.info("Sender has finished work.");
    }

    private void receive() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Thread.sleep(250);
                logger.info("Received: " + blockingQueue.take());
            }
        } catch (InterruptedException ignored) { }
        logger.info("Receiver has finished work.");
    }
}
