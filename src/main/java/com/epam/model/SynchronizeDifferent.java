package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeDifferent {
    private static Logger logger = LogManager.getLogger(SynchronizeOne.class);
    private Object one = new Object();
    private Object two = new Object();
    private Object three = new Object();

    public void first(){
        synchronized (one){
            logger.info("First method start " + Thread.currentThread().getName());
            logger.info("First method finish " + Thread.currentThread().getName());
        }
    }
    public void second(){
        synchronized (two){
            logger.info("Second method start " + Thread.currentThread().getName());
            logger.info("Second method finish " + Thread.currentThread().getName());
        }
    }
    public void third(){
        synchronized (three){
            logger.info("Third method start " + Thread.currentThread().getName());
            logger.info("Third method finish " + Thread.currentThread().getName());
        }
    }
}
