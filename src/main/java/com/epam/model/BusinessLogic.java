package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.concurrent.Executors.*;

public class BusinessLogic implements Model {
    private static Logger logger = LogManager.getLogger(BusinessLogic.class);

    private static void start() {
        logger.info(Thread.currentThread().getName());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info(Thread.currentThread().getName());
    }

    @Override
    public void runPinPong() {
        PingPong pingPong = new PingPong();
        pingPong.run(10);
    }

    @Override
    public void runFibonacci(int size) {
        Fibonacci fibonacci = new Fibonacci(size);
        fibonacci.start();
    }

    @Override
    public void runExecutor() throws InterruptedException {
        ExecutorFibonacci executor = new ExecutorFibonacci();
        logger.info("Single thread executor:");
        executor.run(newSingleThreadExecutor(), 10);
        Thread.sleep(500);
        logger.info("Cached thread pool:");
        executor.run(newCachedThreadPool(), 10);
        Thread.sleep(500);
        logger.info("Fixed thread pool:");
        executor.run(newFixedThreadPool(10), 10);
        Thread.sleep(500);
    }

    @Override
    public void sumFibonacci() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(3);
        logger.info("Sum: " + executor.submit(new SumFibonacci(5)).get());
        Thread.sleep(500);
        logger.info("Sum: " + executor.submit(new SumFibonacci(10)).get());
        Thread.sleep(500);
        logger.info("Sum: " + executor.submit(new SumFibonacci(15)).get());
        Thread.sleep(500);
    }

    @Override
    public void sleepRandom(int quantity) {
        SleepRandom sleepRandom = new SleepRandom(quantity);
        sleepRandom.run();
    }

    @Override
    public void synchronizeOne() throws InterruptedException {
        SynchronizeOne synchronizeOne = new SynchronizeOne();
        Thread first = new Thread(synchronizeOne::first);
        Thread second = new Thread(synchronizeOne::second);
        Thread third = new Thread(synchronizeOne::third);

        first.start();
        second.start();
        third.start();

        first.join();
        second.join();
        third.join();
    }

    @Override
    public void synchronizeDifferent() throws InterruptedException {
        SynchronizeDifferent synchronizeDifferent = new SynchronizeDifferent();
        Thread first = new Thread(synchronizeDifferent::first);
        Thread second = new Thread(synchronizeDifferent::second);
        Thread third = new Thread(synchronizeDifferent::third);

        first.start();
        second.start();
        third.start();

        first.join();
        second.join();
        third.join();
    }

    @Override
    public void runPipeCommunication() {
        try {
            PipedReader pr = new PipedReader();
            PipedWriter pw = new PipedWriter();
            pw.connect(pr);

            Thread thread1 = new Thread(new PipeReaderThread("ReaderThread", pr));
            Thread thread2 = new Thread(new PipeWriterThread("WriterThread", pw));

            thread1.start();
            thread2.start();
        } catch (Exception e) {
            System.out.println("PipeThread Exception: " + e);
        }
    }

    @Override
    public void synchronizeOneMod() throws InterruptedException {
        SynchronizeOneMod synchronizeOneMod = new SynchronizeOneMod();
        Thread first = new Thread(synchronizeOneMod::first);
        Thread second = new Thread(synchronizeOneMod::second);
        Thread third = new Thread(synchronizeOneMod::third);

        first.start();
        second.start();
        third.start();

        first.join();
        second.join();
        third.join();
    }

    @Override
    public void synchronizeDifferentMod() throws InterruptedException {
        SynchronizeDifferentMod synchronizeDifferentMod = new SynchronizeDifferentMod();
        Thread first = new Thread(synchronizeDifferentMod::first);
        Thread second = new Thread(synchronizeDifferentMod::second);
        Thread third = new Thread(synchronizeDifferentMod::third);

        first.start();
        second.start();
        third.start();

        first.join();
        second.join();
        third.join();
    }

    @Override
    public void runBlockingQueue() {
        BlockingQueueTest blockingQueueTest = new BlockingQueueTest();
        blockingQueueTest.startCommunication();
    }

    @Override
    public void testSimpleLock() {
        SimpleLock simpleLock = new SimpleLock();
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                try {
                    simpleLock.lock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                start();
                simpleLock.unlock();
            }).start();
        }
    }
}
