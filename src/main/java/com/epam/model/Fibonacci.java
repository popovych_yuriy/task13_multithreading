package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci extends Thread{
    private static Logger logger = LogManager.getLogger(Fibonacci.class);
    private int size;
    List<Integer> numbers = new ArrayList<>();

    public Fibonacci(int size) {
        this.size = size;
    }

    private void calculate(int n){
        if(n < 2){
            numbers.add(n);
        }else{
            numbers.add(numbers.get(n-2)+numbers.get(n-1));
        }
    }

    @Override
    public void run(){
        for(int i = 0;i < size; i++){
            calculate(i);
        }
        logger.info("Fibonacci numbers: "+ numbers);
    }
}
