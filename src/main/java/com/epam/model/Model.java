package com.epam.model;

import java.util.concurrent.ExecutionException;

public interface Model {
    void runPinPong();

    void runFibonacci(int size);

    void runExecutor() throws InterruptedException;

    void sumFibonacci() throws InterruptedException, ExecutionException;

    void sleepRandom(int quantity);

    void synchronizeOne() throws InterruptedException;

    void synchronizeDifferent() throws InterruptedException;

    void runPipeCommunication();

    void synchronizeOneMod() throws InterruptedException;

    void synchronizeDifferentMod() throws InterruptedException;

    void runBlockingQueue();

    void testSimpleLock();
}
