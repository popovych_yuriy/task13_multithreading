package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Task1 - Run simple “ping-pong” program using wait() and notify().");
        menu.put("2", "  2 - Task2 - Sequence of n Fibonacci numbers");
        menu.put("3", "  3 - Task3 - Use different types of executors.");
        menu.put("4", "  4 - Task4 - Sum the values of all the Fibonacci numbers...");
        menu.put("5", "  5 - Task5 - Sleeps for a random amount of time between 1 and 10 seconds...");
        menu.put("6", "  6 - Task6 - Synchronize on one object");
        menu.put("7", "  7 - Task6 - Synchronize on different objects");
        menu.put("8", "  8 - Task7 - Program in which two tasks use a pipe to communicate");
        menu.put("9", "  9 - Task2.1 - Rewritten previous 6 task using use explicit Lock objects");
        menu.put("10", "  10 - Task2.2 - BlockingQueue");
        menu.put("11", "  11 - Task2.3 - Test my SimpleLock");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::runPinPong);
        methodsMenu.put("2", this::runFibonacci);
        methodsMenu.put("3", this::differentExecutors);
        methodsMenu.put("4", this::sumFibonacci);
        methodsMenu.put("5", this::sleepRandom);
        methodsMenu.put("6", this::synchronizeOne);
        methodsMenu.put("7", this::synchronizeDifferent);
        methodsMenu.put("8", this::pipeCommunication);
        methodsMenu.put("9", this::explicitLock);
        methodsMenu.put("10", this::blockingQueue);
        methodsMenu.put("11", this::testLock);
    }

    private void runPinPong() {
        controller.runPinPong();
    }

    private void runFibonacci() {
        controller.runFibonacci(10);
        controller.runFibonacci(6);
    }

    private void differentExecutors() {
        try {
            controller.runExecutor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sumFibonacci() {
        try {
            controller.sumFibonacci();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sleepRandom() {
        controller.sleepRandom(5);
    }

    private void synchronizeOne() {
        try {
            controller.synchronizeOne();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void synchronizeDifferent() {
        try {
            controller.synchronizeDifferent();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void pipeCommunication() {
        controller.runPipeCommunication();
    }

    private void explicitLock() {
        try {
            logger.info("One Lock: ");
            controller.synchronizeOneMod();
            logger.info("Different Locks: ");
            controller.synchronizeDifferentMod();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void blockingQueue() {
        controller.runBlockingQueue();
    }

    private void testLock() {
        controller.testSimpleLock();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
