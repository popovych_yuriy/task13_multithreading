package com.epam.controller;

import java.util.concurrent.ExecutionException;

public interface Controller {
    void runPinPong();

    void runFibonacci(int size);

    void runExecutor() throws InterruptedException;

    void sumFibonacci() throws ExecutionException, InterruptedException;

    void sleepRandom(int quantity);

    void synchronizeOne() throws InterruptedException;

    void synchronizeDifferent() throws InterruptedException;

    void runPipeCommunication();

    void synchronizeOneMod() throws InterruptedException;

    void synchronizeDifferentMod() throws InterruptedException;

    void runBlockingQueue();

    void testSimpleLock();
}
