package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

import java.util.concurrent.ExecutionException;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void runPinPong() {
        model.runPinPong();
    }

    @Override
    public void runFibonacci(int size) {
        model.runFibonacci(size);
    }

    @Override
    public void runExecutor() throws InterruptedException {
        model.runExecutor();
    }

    @Override
    public void sumFibonacci() throws ExecutionException, InterruptedException {
        model.sumFibonacci();
    }

    @Override
    public void sleepRandom(int quantity) {
        model.sleepRandom(quantity);
    }

    @Override
    public void synchronizeOne() throws InterruptedException {
        model.synchronizeOne();
    }

    @Override
    public void synchronizeDifferent() throws InterruptedException {
        model.synchronizeDifferent();
    }

    @Override
    public void runPipeCommunication() {
        model.runPipeCommunication();
    }

    @Override
    public void synchronizeOneMod() throws InterruptedException {
        model.synchronizeOneMod();
    }

    @Override
    public void synchronizeDifferentMod() throws InterruptedException {
        model.synchronizeDifferentMod();
    }

    @Override
    public void runBlockingQueue() {
        model.runBlockingQueue();
    }

    @Override
    public void testSimpleLock() {
        model.testSimpleLock();
    }
}